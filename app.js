const express = require('express');
const app = express();
const blogRouter = require("./routes/BlogRoutes");

const port = 3002

app.use(express.json());
app.use("/api/blogs", blogRouter);

app.listen(port, () =>{
    console.log("Currently running on port " + port);
});

module.exports = app;


const mongoose = require("mongoose");

mongoose.connect(
    process.env.MONGODB_URI || "mongodb://localhost/CRUD",
    {
        useNewURLParser: true,
        useUnifiedTopolofy: true,
    },
    (err) => {
        if (err){
            console.log(err);
        } else {
            console.log("Connected to MongoDB");
        }
    }
)
